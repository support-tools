# Install required packages
R
install.packages("ggplot2")
quit()

# Run graphing script
R --slave -f new-tickets-by-queue.R --args data/2013Q4.csv 2013Q4.pdf

