# Import required packages
require(ggplot2)
require(RColorBrewer)

# Use English locale
Sys.setenv(LANGUAGE="en")
Sys.setlocale("LC_TIME", "en_US.UTF-8")

# Read the csv file passed as argument, but don't convert strings to factors
args <- commandArgs(trailingOnly = TRUE)
data <- read.csv(args[1], stringsAsFactors = FALSE)

# Format months as "month_name year", rather than "year-month"
data$month <- factor(data$month,
  labels = format(as.Date(paste(unique(data$month), "-01", sep = ""),
  format = "%Y-%m-%d"), "%B %Y"))

# Put requests into response time buckets from 0 to 4 hours, 4 to 12
# hours, etc.
data <- aggregate(list(requests = data$requests),
  list(interval = cut(data$tohours,
  breaks = c(0, 4, 12, 24, 48, 96, 168, max(data$tohours)),
  labels = c("Less than 4 hours", "4 to 12 hours", "12 to 24 hours",
             "1 to 2 days", "2 to 4 days", "4 days to 1 week",
             "More than 1 week")),
  month = data$month), sum)

# Plot the data
ggplot(data,
  aes(x = factor(1),              # Single value on x axis
      y = requests,               # Number of requests on y axis
      fill = interval)) +         # Different colours for response time
geom_bar(                         # Start with a bar plot
  stat = "identity",              # Make y represent values in the data
  position = "fill",              # Make all bars the same length
  width = 1) +                    # Make all pies the same size
facet_grid(. ~ month) +           # Make 1 chart per month
coord_polar(theta = "y") +        # Change y coordinates for pie chart
scale_x_discrete("") +            # No x axis label
scale_y_continuous("") +          # No y axis label
# No legend title
# For other color palettes, see http://colorbrewer2.org/ and
# http://vis.supstat.com/2013/04/plotting-symbols-and-color-palettes/
scale_fill_brewer("", palette = "Set2") +
ggtitle("Time to respond to support requests per month\n") +
theme_bw() +
theme(axis.text = element_blank(),        # No tick labels
      axis.ticks = element_blank(),       # No ticks
      panel.grid.major = element_blank()) # No axis lines
ggsave(args[2], width = 8, height = 4, dpi = 100)
